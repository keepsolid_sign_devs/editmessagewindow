import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2

Window {

    id: editMessageWindow
    visible: true
    width: 600
    height: 308
    minimumWidth: 600
    minimumHeight: 308
    maximumWidth: 600
    maximumHeight: 308
    modality: Qt.WindowModal
    color: "#ffffff"
    title: qsTr("Send document to signers")

    FontLoader {

        id: openSansBold
        source: "OpenSansFonts/OpenSans-Bold.ttf"
    }

    FontLoader {

        id: openSansRegular
        source: "OpenSansFonts/OpenSans-Regular.ttf"
    }

    Rectangle {
        id: rectangle
        color: "#fcfeff"
        anchors.rightMargin: 20
        anchors.leftMargin: 20
        anchors.bottomMargin: 20
        anchors.top: messageText.bottom
        anchors.topMargin: 10
        border.color: "#a7cdef"
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: button.top

        Flickable {

            id: flickableText
            anchors.fill: parent
            flickableDirection: Flickable.VerticalFlick
            contentHeight: textArea.height
            clip: true
            Keys.onUpPressed: scrollBar.decrease()
            Keys.onDownPressed: scrollBar.increase()
            TextArea.flickable: TextArea {
                id: textArea
                text: qsTr("Text Area")
                padding: 0
                rightPadding: 12
                leftPadding: 12
                bottomPadding: 9
                topPadding: 9
                font.pointSize: 12
                wrapMode: TextEdit.Wrap
                font.family: openSansRegular.name
                color: "#444444"
                selectionColor: "#a7cdef"
                selectByMouse: true
            }
            ScrollBar.vertical: ScrollBar{

                id: scrollBar
                parent: flickableText.parent
                width: 10
                anchors.top: parent.top
                anchors.topMargin: 1
                anchors.right: parent.right
                anchors.rightMargin: 1
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                hoverEnabled: true
                active: hovered || pressed
                orientation: Qt.Vertical
                background: Rectangle {

                    id: scrollBarBackground
                    anchors.fill: parent
                    color: "#eaeaea"
                    visible: scrollBar.active
                }
                contentItem: Rectangle {

                    id: scrollBarInterator
                    color: "#2980cc"
                    visible: scrollBar.active
                }
            }
        }
    }

    Text {
        id: messageText
        height: 14
        text: qsTr("Message")
        color: "#444444"
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 24
        font.pointSize: 14
        font.family: openSansRegular.name
    }

    Button {
        id: button
        x: 480
        y: 256
        width: 120
        height: 36
        text: qsTr("Save")
        font.pointSize: 13
        font.family: openSansRegular.name
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        background: Rectangle {

            anchors.fill: parent
            color: "#2980cc"
        }
        contentItem: Label {

            anchors.fill: parent
            text: parent.text
            font: parent.font
            color: "#ffffff"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        onClicked: {

            console.log("Save button was clicked, have to be implemented handler")
        }
    }

}
